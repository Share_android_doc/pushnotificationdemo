package com.example.kimsoerhrd.pushnotificationdemo.entity.data;

import com.google.gson.annotations.SerializedName;

public class Content {
    @SerializedName("en")
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "Content{" +
                "title='" + title + '\'' +
                '}';
    }
}
