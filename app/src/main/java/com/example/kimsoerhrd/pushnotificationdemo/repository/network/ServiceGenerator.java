package com.example.kimsoerhrd.pushnotificationdemo.repository.network;

import java.io.IOException;


import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ServiceGenerator {
   static Retrofit.Builder builder = new Retrofit.Builder()
            .baseUrl("https://onesignal.com/")
            .addConverterFactory(GsonConverterFactory.create());
    public static <S> S createService(Class<S> service){
        final OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
         httpClient.addInterceptor(new Interceptor() {
             @Override
             public Response intercept(Chain chain) throws IOException {
                 Request original = chain.request();
                 Request.Builder requestBuilder = original.newBuilder()
                         .addHeader("Authorization","Basic NDE0NWJkNDYtM2U5Yy00MTcyLTg0ZGEtY2FhY2ZjNzkyZTlj")
                         .header("Content-Type","application/json")
                         .method(original.method(),original.body());
                 Request request = requestBuilder.build();

                 return chain.proceed(request);
             }
         });
        OkHttpClient client = httpClient.build();
        Retrofit retrofit = builder.client(client).build();
        return retrofit.create(service);

    }


}
