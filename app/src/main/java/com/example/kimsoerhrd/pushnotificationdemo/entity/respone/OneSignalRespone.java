package com.example.kimsoerhrd.pushnotificationdemo.entity.respone;


import com.example.kimsoerhrd.pushnotificationdemo.entity.data.Content;
import com.example.kimsoerhrd.pushnotificationdemo.entity.data.DataEntity;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OneSignalRespone {

    @SerializedName("contents")
    private Content contents;
    @SerializedName("data")
    private DataEntity data;
    @SerializedName("included_segments")
    private List<String> included_segments;
    @SerializedName("app_id")
    private String app_id;

    public Content getContents() {
        return contents;
    }

    public void setContents(Content contents) {
        this.contents = contents;
    }

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public List<String> getIncluded_segments() {
        return included_segments;
    }

    public void setIncluded_segments(List<String> included_segments) {
        this.included_segments = included_segments;
    }

    public String getApp_id() {
        return app_id;
    }

    public void setApp_id(String app_id) {
        this.app_id = app_id;
    }

    @Override
    public String toString() {
        return "OneSignalRespone{" +
                "contents=" + contents +
                ", data=" + data +
                ", included_segments=" + included_segments +
                ", app_id='" + app_id + '\'' +
                '}';
    }
}
