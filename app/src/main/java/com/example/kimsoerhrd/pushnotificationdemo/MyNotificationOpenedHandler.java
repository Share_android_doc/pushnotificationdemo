package com.example.kimsoerhrd.pushnotificationdemo;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.onesignal.OSNotification;
import com.onesignal.OSNotificationAction;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import org.json.JSONObject;

public class MyNotificationOpenedHandler implements OneSignal.NotificationOpenedHandler {
    Context context;

    public MyNotificationOpenedHandler(Context context) {
        this.context = context;
    }
    @Override
    public void notificationOpened(OSNotificationOpenResult result) {
        OSNotificationAction.ActionType actionType = result.action.type;
        JSONObject data = result.notification.payload.additionalData;
        String customkey="";
        if (data != null){
            customkey = data.optString("customkey", null);
            if (customkey != null){
                Log.e("OneSignalExample", "customkey set with value :"+customkey);
            }
        }
        if (actionType == OSNotificationAction.ActionType.ActionTaken)
            Log.e("OneSignalExample","Button pressed with id: "+result.action.actionID);
            Intent intent = new Intent(context.getApplicationContext(), Main2Activity.class);
            intent.putExtra("data", result.notification.payload.body);

            context.startActivity(intent);
    }



}
