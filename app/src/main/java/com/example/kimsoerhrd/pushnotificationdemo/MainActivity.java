package com.example.kimsoerhrd.pushnotificationdemo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.kimsoerhrd.pushnotificationdemo.entity.OneSignalMessage;
import com.example.kimsoerhrd.pushnotificationdemo.entity.data.Content;
import com.example.kimsoerhrd.pushnotificationdemo.entity.data.DataEntity;
import com.example.kimsoerhrd.pushnotificationdemo.entity.respone.OneSignalRespone;
import com.example.kimsoerhrd.pushnotificationdemo.repository.OneSignalService;
import com.example.kimsoerhrd.pushnotificationdemo.repository.network.ServiceGenerator;
import com.onesignal.OneSignal;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    Button btn;
    TextView textView;
        OneSignalService signalService;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        signalService = ServiceGenerator.createService(OneSignalService.class);

    }


    public void onPushNotification(View view) {
        OneSignalRespone message = new OneSignalRespone();
        message.setApp_id("365db9d5-7dd4-4445-b817-f61bf68c9ecb");
        List<String> users=new ArrayList();
        users.add("All");
        message.setIncluded_segments(users);
        Content content = new Content();
        content.setTitle("Kimsoer well");
        message.setContents(content);
        DataEntity entity = new DataEntity();
        entity.setFoo("foo");
        message.setData(entity);

        Call<OneSignalRespone> call = signalService.pushNotification(message);
        call.enqueue(new Callback<OneSignalRespone>() {
            @Override
            public void onResponse(Call<OneSignalRespone> call, Response<OneSignalRespone> response) {

            }

            @Override
            public void onFailure(Call<OneSignalRespone> call, Throwable t) {

            }
        });


    }
}
