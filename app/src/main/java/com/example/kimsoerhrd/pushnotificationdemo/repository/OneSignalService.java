package com.example.kimsoerhrd.pushnotificationdemo.repository;


import com.example.kimsoerhrd.pushnotificationdemo.entity.respone.OneSignalRespone;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface OneSignalService  {

    @POST("api/v1/notifications")
    Call<OneSignalRespone> pushNotification(@Body OneSignalRespone title);
}
