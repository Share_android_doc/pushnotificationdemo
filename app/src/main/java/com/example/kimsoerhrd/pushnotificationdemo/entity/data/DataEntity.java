package com.example.kimsoerhrd.pushnotificationdemo.entity.data;

import com.google.gson.annotations.SerializedName;

public class DataEntity {
    @SerializedName("foo")
    private String foo;

    public String getFoo() {
        return foo;
    }

    public void setFoo(String foo) {
        this.foo = foo;
    }

    @Override
    public String toString() {
        return "DataEntity{" +
                "foo='" + foo + '\'' +
                '}';
    }
}
