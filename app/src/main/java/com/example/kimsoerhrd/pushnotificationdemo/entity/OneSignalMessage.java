package com.example.kimsoerhrd.pushnotificationdemo.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class OneSignalMessage {

    @SerializedName("data")
    private DataEntity data;
    @SerializedName("included_segments")
    private List<String> included_segments;
    @SerializedName("app_id")
    private String app_id;

    public DataEntity getData() {
        return data;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public List<String> getIncluded_segments() {
        return included_segments;
    }

    public void setIncluded_segments(List<String> included_segments) {
        this.included_segments = included_segments;
    }

    public String getApp_id() {
        return app_id;
    }

    public void setApp_id(String app_id) {
        this.app_id = app_id;
    }

    public static class DataEntity {
        @SerializedName("title")
        private String title;

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }
}
